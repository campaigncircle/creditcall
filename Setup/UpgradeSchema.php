<?php

namespace CCircle\Creditcall\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Psr\Log\LoggerInterface;

/*
* $connection->addColumn(
*        $tableName,
*        'card_num',
*        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
*        null,
*        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
*        'Card Number'
*    );
*
*    $connection->changeColumn(
*        $tableName,
*        'summary',
*        'short_summary',
*        ['type' => Table::TYPE_TEXT, 'nullable' => false, 'default' => ''],
*        'Short Summary'
*    );  
* */

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        
       
        $cardsEntityTable = 'ccircle_creditcall_cards';
        

        $setup->startSetup();
        $tableName = $setup->getTable($cardsEntityTable);

        if (version_compare($context->getVersion(), '1.0.0') < 0) {
            // Changes here.
        }

        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            // Changes here.
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {

            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
               
                $connection->addColumn(
                    $tableName,
                    'card_num',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Card Number'
                );
            }
        }
         if (version_compare($context->getVersion(), '1.0.3', '<')) {
            echo '1.0.3';
         }
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            echo '1.0.4';
            $setup->getConnection()->dropColumn($tableName, 'card_num');
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            echo '1.0.5';
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
               
                $connection->addColumn(
                    $tableName,
                    'card_num',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Card Number'
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            echo 'Installing v1.0.6';
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
               
                $connection->addColumn(
                    $tableName,
                    'card_exp',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['length'  => 5,'nullable' => false, 'default' => '12/25'],
                    'Card Number'
                );
            }
        }

        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            echo 'Installing v1.0.7';
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
               
                $connection->addColumn(
                    $tableName,
                    'order_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => false],
                    'Order Id'
                );
            }
        }

        $setup->endSetup();

    }
}
