magento2-CCircle_Creditcall
======================

Creditcall payment gateway Magento2 extension


Install
=======

1. Go to Magento2 root folder

2. Install using composor

composer require ccircle/creditcall


3. Enter following commands to enable module:

    ```bash
    php bin/magento module:enable CCircle_Creditcall --clear-static-content
    php bin/magento setup:upgrade
    ```
4. Enable and configure Creditcall in Magento Admin under Stores/Configuration/Payment Methods/Creditcall

Other Notes
===========


