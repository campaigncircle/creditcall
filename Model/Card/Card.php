<?php

namespace CCircle\Creditcall\Model\Card;

class Card
{
    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
   */  
    protected $customerSession;
    protected $_dateTime;
    protected $_cardFactory;

    /**
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \CCircle\Creditcall\Model\CardsFactory $cardsFactory
    )
    {
       $this->_logger = $logger;
       $this->_logger->debug('Start createCard');
        $this->_dateTime = $dateTime;
        $this->_cardsFactory = $cardsFactory;
        $this->_customerSession = $customerSession;
        $this->_dateTime = $dateTime;
        //parent::__construct($customerSession);
    }

    /**
     * Check customer authentication for some actions
     */
    public function authenticated()
    {
        
        if (!$this->_customerSession->authenticate()) {
            return false;
        }
        return true;
        /**/
    }

    public function saveCard($cardHash, $cardRef, $orderID, $cardExp)
    {
        
        try {

            if($this->authenticated()){
                
                $this->_logger->debug('Start createCard->save');
                $card = $this->_cardsFactory->create();
                //$card->load($this->_customerSession->getCustomerId(), 'customer_id');
                $card->setCustomerId($this->_customerSession->getCustomerId());
                $card->setHash($cardHash);
                $card->setRef($cardRef);
                //$card->setCardExp($cardExp);
                $card->setOrderId($orderID);
                $card->setCreatedAt($this->_dateTime->formatDate(true));
                $card->save();

                

               $this->_logger->debug('Inside CCircle\Creditcall\Model\Card, Card Saved');
            }else{
              $this->_logger->debug('Inside CCircle\Creditcall\Model\Card, User not Authenticated');
            }

        } catch (Exception $e) {
            $this->_logger->debug('Inside CCircle\Creditcall\Model\Card, Error: '.$e->getMessage());
        }

        return true;
       /* */
    }

    public function getCard(){
        try {
            if($this->authenticated()){
                $card = $this->_cardsFactory->create();
                $theCard = $card->load($this->_customerSession->getCustomerId(), 'customer_id');
                if($theCard){
                    return $theCard;
                }
            }  
            return false;

         } catch (Exception $e) {
            $this->_logger->debug('Inside CCircle\Creditcall\Model\Card, Error: '.$e->getMessage());
        }
    }
}
