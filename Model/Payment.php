<?php
/**
* Creditcall payment method model
*
* @category    CCircle
* @package     CCircle_Creditcall
* @author      Steve Winnington
* @copyright   CCircle (http://CampaignCircle.co.uk)
* @license     http://CampaignCircle.co.uk/licenses/1.0.php  Propretory Software License (CC 1.0)
 */

namespace CCircle\Creditcall\Model;

use Omnipay\Omnipay;


class Payment extends \Magento\Payment\Model\Method\Cc
{
    const CODE = 'ccircle_creditcall';

    private $_cardHash                      = array();

    protected $_code = self::CODE;

    protected $_isGateway                   = true;
    protected $_canCapture                  = true;
    protected $_canCapturePartial           = true;
    protected $_canRefund                   = true;
    protected $_canRefundInvoicePartial     = true;

    protected $_creditcallApi = false;

    protected $_countryFactory;
    protected $_card;

    protected $_minAmount = null;
    protected $_maxAmount = null;
    protected $_supportedCurrencyCodes = array('USD','GBP');

    protected $_debugReplacePrivateDataKeys = ['number', 'exp_month', 'exp_year', 'cvc'];

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\Encryption\Encryptor $crypt,
        \CCircle\Creditcall\Model\Card\Card $card,
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            null,
            $data
        );

        $this->_card = $card;
        $this->_countryFactory = $countryFactory;
        $this->_minAmount = $this->getConfigData('min_order_total');
        $this->_maxAmount = $this->getConfigData('max_order_total');

        $this->_terminal_id = $this->getConfigData('terminal_id');
        $this->_transaction_key = $this->getConfigData('transaction_key');

        $this->_creditcallApi = Omnipay::create('Creditcall');
        $this->_creditcallApi->setTerminalId( $this->_terminal_id );
        $this->_creditcallApi->setTransactionKey( $this->_transaction_key );
        $this->_creditcallApi->setTestMode(true);


        //$this->testCapture('authorise');
        /** TEST CARD HASH
        $this->_cardHash  = array(
                    'mastercard' => 
                        array(
                            'hash' => 'ZYmw1GtvLw26nrqxby3Q/0mYaPQ=',
                            'ref' => '46a06c79-913f-e611-b102-002219649f24',
                        ),
                    'visa' => 
                        array(
                            'hash' => 'YhY6AXsWitSiKcZK4b7W/9Xo+y0=',
                            'ref' => 'ab4bc119-8425-e011-80a6-001422187e37',
                        )
                );
        **/
        //$this->testCardHash();
        //$this->testCapture();
        
       // if($this->_card->getCard()){
           // echo "<h1>User Card Exists</h1>";
           
        //}

    }


    /**
     * Authorize payment abstract method
     *
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     * 
     */
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {   
        /** @var \Magento\Sales\Model\Order $order */
        $order = $payment->getOrder();
        $paymentField = $order->getPayment();
        $invoiceNo = $order->getOrderIncrementId();

        /** @var \Magento\Sales\Model\Order\Address $billing */
        $billing = $order->getBillingAddress();

        $response = $this->_creditcallApi->authorize($requestData)->send();



        try {
            $requestData = [
                'amount'        => $amount,
                'currency'      => $order->getBaseCurrencyCode(),
                'description'   => sprintf('#%s, %s', $order->getIncrementId(), $order->getCustomerEmail()),
                'transactionId' => "#".$order->getIncrementId(),
                'card'          => [
                    'number'            => $payment->getCcNumber(),
                    'expiryMonth'         => sprintf('%02d',$payment->getCcExpMonth()),
                    'expiryYear'          =>  substr($payment->getCcExpYear(), 2),
                    'cvv'               => $payment->getCcCid(),
                    'name'              => $billing->getName(),
                    'firstName'         => $billing->getFirstname(),
                    'lastName'         => $billing->getLastname(),
                    'billingAddress1'     => $billing->getStreetLine(1),
                    'billingAddress2'     => $billing->getStreetLine(2),
                    'billingCity'      => $billing->getCity(),
                    'billingPostcode'       => $billing->getPostcode(),
                    'billingState'     => $billing->getRegion(),
                    'billingCountry'   => $billing->getCountryId()
                ]
            ];

            $response = $this->_creditcallApi->authorize($requestData)->send();

            if ($response->isSuccessful()) {
                 $this->_logger->debug('Inside Creditcall authorize, authorization was successful: update database');

            } elseif ($response->isRedirect()) {
                // redirect to offsite payment gateway
                throw new \Magento\Framework\Validator\Exception(__('Payment error: redirect to offsite payment gateway'));
               $this->_logger->debug('Inside Creditcall authorize, redirect to offsite payment gateway');
            } else {
                // payment failed: display message to customer
                throw new \Magento\Framework\Validator\Exception(__('Payment error: '.$response->getMessage()));
                $this->_logger->debug('Inside Creditcall authorize, payment failed: display message to customer '.$amount.': '.$response->getMessage());
            } 

            //$payment
            //   ->setTransactionId($response->getTransactionReference())
            //    ->setIsTransactionClosed(0);

            /** @var \Magento\Sales\Model\Order\Payment $payment */
            $payment->setStatus(self::STATUS_APPROVED)
                ->setCcTransId($response->getTransactionReference())
                ->setLastTransId($response->getTransactionReference())
                ->setTransactionId($response->getTransactionReference())
                ->setIsTransactionClosed(false)
                //->setCcLast4($result->transaction->creditCardDetails->last4)
                // ->setAdditionalInformation($this->getExtraTransactionInformation($result->transaction))
                ->setAmount($amount)
                ->setShouldCloseParentTransaction(false);

        } catch (\Exception $e) {
            $this->debugData(['request' => $requestData, 'exception' => $e->getMessage()]);
            $this->_logger->error(__('Payment capturing error.'));
            throw new \Magento\Framework\Validator\Exception(__('Payment capturing error.'));
        }

        return $this;


    }


    /**
     * Payment Authorize and capturing
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Validator\Exception
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {

        //$saveCardCheck = true;
        $this->_logger->debug('Inside Creditcall capture: '.$payment->getOrderComment());
        //if($this->_card->getCard() && $saveCardCheck){    
        //}


        /** @var \Magento\Sales\Model\Order $order */
        $order = $payment->getOrder();
        $paymentField = $order->getPayment();

        //$paymentSaveCard = $paymentField->getSaveCard();
        //$this->_logger->debug('Testing Payment Form SaveCard: '.$paymentSaveCard);
        //throw new \Magento\Framework\Validator\Exception(__('Payment capturing error.'));

        $invoiceNo = $order->getOrderIncrementId();

        //$this->_logger->debug('Inside Creditcall capture, invoiceNo: '.$invoiceNo);

        /** @var \Magento\Sales\Model\Order\Address $billing */
        $billing = $order->getBillingAddress();

        $this->_logger->debug('Inside Creditcall capture, getBillingAddress');
 
        try {
            $requestData = [
                'amount'        => $amount,
                'currency'      => $order->getBaseCurrencyCode(),
                'description'   => sprintf('#%s, %s', $order->getIncrementId(), $order->getCustomerEmail()),
                'transactionId' => "#".$order->getIncrementId(),
                'card'          => [
                    'number'            => $payment->getCcNumber(),
                    'expiryMonth'         => sprintf('%02d',$payment->getCcExpMonth()),
                    'expiryYear'          => substr($payment->getCcExpYear(), 2),
                    'cvv'               => $payment->getCcCid(),
                    'name'              => $billing->getName(),
                    'firstName'         => $billing->getFirstname(),
                    'lastName'         => $billing->getLastname(),
                    'billingAddress1'     => $billing->getStreetLine(1),
                    'billingAddress2'     => $billing->getStreetLine(2),
                    'billingCity'      => $billing->getCity(),
                    'billingPostcode'       => $billing->getPostcode(),
                    'billingState'     => $billing->getRegion(),
                    'billingCountry'   => $billing->getCountryId(),
                    // To get full localized country name, use this instead:
                    // 'address_country'   => $this->_countryFactory->create()->loadByCode($billing->getCountryId())->getName(),
                ]
            ];

            //$this->_logger->debug(sprintf('#%s, %s, %02d, %d', $order->getIncrementId(), $order->getCustomerEmail(), $payment->getCcExpMonth(), substr($payment->getCcExpYear(), 2)));


            /* SET METHOD TO AUTHORIZE NOT CAPTURE */
            //$response = $this->_creditcallApi->purchase($requestData)->send();
            $response = $this->_creditcallApi->authorize($requestData)->send();

            $this->_logger->debug('Inside Creditcall capture, running Purchase');

            if ($response->isSuccessful()) {
                 $this->_logger->debug('Inside Creditcall capture, payment was successful: '. var_dump( $response));

                 $cardExp = sprintf('%02d',$payment->getCcExpMonth())."/".substr($payment->getCcExpYear(), 2);

                 $this->_card->saveCard($response->getCardHash(), $response->getCardReference(), $order->getIncrementId(), $cardExp);
                 $this->_logger->debug('Finished Saving card');

            } elseif ($response->isRedirect()) {
                // redirect to offsite payment gateway
                throw new \Magento\Framework\Validator\Exception(__('Payment error: redirect to offsite payment gateway'));
               $this->_logger->debug('Inside Creditcall capture, redirect to offsite payment gateway');
            } else {
                // payment failed: display message to customer
                throw new \Magento\Framework\Validator\Exception(__('Payment error: '.$response->getMessage()));
                $this->_logger->debug('Inside Creditcall capture, payment failed: display message to customer '.$amount.': '.$response->getMessage());
            } 

            $payment
                ->setTransactionId($response->getTransactionReference())
                ->setIsTransactionClosed(0);

        } catch (\Exception $e) {
            $this->debugData(['request' => $requestData, 'exception' => $e->getMessage()]);
            $this->_logger->error(__('Payment capturing error.'));
            throw new \Magento\Framework\Validator\Exception(__('Payment capturing error.'));
        }

        return $this;
       
    }

    /**
     * Payment refund
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Validator\Exception
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        /* */
        $this->_logger->debug('Refund Command Running');
        $transactionId = $payment->getParentTransactionId();

        try {
            \Creditcall\Charge::retrieve($transactionId)->refund(['amount' => $amount * 100]);
        } catch (\Exception $e) {
            $this->debugData(['transaction_id' => $transactionId, 'exception' => $e->getMessage()]);
            $this->_logger->error(__('Payment refunding error.'));
            throw new \Magento\Framework\Validator\Exception(__('Payment refunding error.'));
        }

        $payment
            ->setTransactionId($transactionId . '-' . \Magento\Sales\Model\Order\Payment\Transaction::TYPE_REFUND)
            ->setParentTransactionId($transactionId)
            ->setIsTransactionClosed(1)
            ->setShouldCloseParentTransaction(1);

        return $this;
       
    }

    /**
     * Determine method availability based on quote amount and config data
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        /* */
        //$this->_logger->debug('isAvailable Command Running');
        if ($quote && (
            $quote->getBaseGrandTotal() < $this->_minAmount
            || ($this->_maxAmount && $quote->getBaseGrandTotal() > $this->_maxAmount))
        ) {
            return false;
        }

        if (!$this->getConfigData('terminal_id')) {
            return false;
        }

        return parent::isAvailable($quote);
       
    }

    /**
     * Availability for currency
     *
     * @param string $currencyCode
     * @return bool
     */
    public function canUseForCurrency($currencyCode)
    {   
         //$this->_logger->debug('canUseForCurrency Command Running');
        /* */
        if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
            return false;
        }
        return true;
       
    }
   
   private function frand($min, $max, $decimals = 0) {
        $scale = pow(10, $decimals);
        return mt_rand($min * $scale, $max * $scale) / $scale;
    } 

    private function testCardHash(){
        $transactionId = mt_rand(10000000, 20000000);
        $amount = $this->frand(10, 129, 2);

        $this->_card->saveCard($this->_cardHash['visa']['hash'], $this->_cardHash['visa']['ref']);
        $this->_logger->debug('Finished Saving card');

        $testHash = [
                'amount'        => $amount,
                'currency'      => "GBP",
                'description'   => "Test Transaction",
                'transactionId' => "#".$transactionId,
                'cardReference' => $this->_cardHash['visa']['ref'],
                'cardHash'      => $this->_cardHash['visa']['hash']
            ];

        $response = $this->_creditcallApi->purchase($testHash)->send();

         $this->processResponse($response);
    }

    private function testCapture($type = 'capture'){

        $transactionId = mt_rand(10000000, 20000000);
        $amount = $this->frand(10, 129, 2);
 
        $testData = [
                'amount'        => $amount,
                'currency'      => "GBP",
                'description'   => "This is a Test Transaction",
                'transactionId' => "#".$transactionId,
                'card'          => [
                    'number'            => "4012888888881881",
                    'expiryMonth'         => "04",
                    'expiryYear'          => "24",
                    'cvv'               => "123",
                    'name'              => "Steve Winnington",
                    'firstName'         => "Steve",
                    'lastName'         => "Winnington",
                    'billingAddress1'     => "A House",
                    'billingAddress2'     => "A Street",
                    'billingCity'      => "A Town",
                    'billingPostcode'       => "BH21 3TZ",
                    'billingState'     => "Dorset",
                    'billingCountry'   => "UK",
                    // To get full localized country name, use this instead:
                    // 'address_country'   => $this->_countryFactory->create()->loadByCode($billing->getCountryId())->getName(),
                ]
            ];
            
            if($type == 'capture'){
                $response = $this->_creditcallApi->purchase($testData)->send();
            }else{
                $response = $this->_creditcallApi->authorize($testData)->send();
            }

           $this->processResponse($response, $amount, $transactionId);
    }

    private function processResponse($response, $amount, $transactionId){
         if ($response->isSuccessful()) {
                // payment was successful: update database
                echo "<h1>Sucess</h1>" ;
                echo "<p>Amount: ".$amount."";
                echo "<p>ID: ".$response->getTransactionReference()."</p>"; // a reference generated by the payment gateway
                echo "<p>Trans: ".$transactionId."</p>"; // the reference set by the originating website if available.
                echo "<p>Mess: ".$response->getMessage()."</p>"; // a message generated by the payment gateway
                echo "<p>Hash: ".$response->getCardHash()."</p>"; // a card hash generated by the payment gateway
                echo "<p>Ref: ".$response->getCardReference()."</p>"; // a card ref generated by the payment gateway
                $this->_card->saveCard($response->getCardHash(), $response->getCardReference(), $transactionId);
            } elseif ($response->isRedirect()) {
                // redirect to offsite payment gateway
                //$response->redirect();
                echo "<h1>Redirect</h1>" ;

            } else {
                // payment failed: display message to customer
                echo "<h1>Error</h1>" ;
                echo $response->getMessage();
           }
    }
}