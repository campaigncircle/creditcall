<?php

namespace CCircle\Creditcall\Model;

class Cards extends \Magento\Framework\Model\AbstractModel
{

    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {	
        $this->_init('CCircle\Creditcall\Model\ResourceModel\Cards');
    }

}