<?php

namespace CCircle\Creditcall\Model\ResourceModel\Cards;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Constructor
     * Configures collection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CCircle\Creditcall\Model\Cards', 'CCircle\Creditcall\Model\ResourceModel\Cards');
    }
}
