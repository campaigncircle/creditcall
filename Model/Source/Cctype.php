<?php
/**
 * Payment CC Types Source Model
 *
 * @category    CCircle
 * @package     CCircle_Creditcall
* @author      Steve Winnington
* @copyright   CCircle (http://CampaignCircle.co.uk)
* @license     http://CampaignCircle.co.uk/licenses/1.0.php  Propretory Software License (CC 1.0)
 */

namespace CCircle\Creditcall\Model\Source;

class Cctype extends \Magento\Payment\Model\Source\Cctype
{
    /**
     * @return array
     */
    public function getAllowedTypes()
    {
        return array('VI', 'VD', 'MC', 'AE', 'OT');
    }
}
