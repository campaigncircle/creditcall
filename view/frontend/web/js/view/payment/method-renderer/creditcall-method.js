/**
* CCircle_Creditcall Magento JS component
*
* @category    CCircle
* @package     CCircle_Creditcall
* @author      Steve Winnington
* @copyright   CCircle (http://CampaignCircle.co.uk)
* @license     http://CampaignCircle.co.uk/licenses/1.0.php  Propretory Software License (CC 1.0)
*/
/*browser:true*/
/*global define*/
define(
    [
        'Magento_Payment/js/view/payment/cc-form',
        'jquery',
        'Magento_Payment/js/model/credit-card-validation/validator'
    ],
    function (Component, $) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'CCircle_Creditcall/payment/creditcall-form'
            },

            getCode: function() {
                return 'ccircle_creditcall';
            },

            isActive: function() {
                return true;
            },

            validate: function() {
                var $form = $('#' + this.getCode() + '-form');
                return $form.validation() && $form.validation('isValid');
            }
        });
    }
);
