/**
* CCircle_Creditcall Magento JS component
*
* @category    CCircle
* @package     CCircle_Creditcall
* @author      Steve Winnington
* @copyright   CCircle (http://CampaignCircle.co.uk)
* @license     http://CampaignCircle.co.uk/licenses/1.0.php  Propretory Software License (CC 1.0)
*/
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'ccircle_creditcall',
                component: 'CCircle_Creditcall/js/view/payment/method-renderer/creditcall-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);